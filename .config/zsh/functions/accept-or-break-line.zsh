# Insert linebreak on unfinished command,
# accept command on finished command
function accept-or-break-line {
	{
		unfunction _al_f_
		functions[_al_f_]=$BUFFER
	} 2> /dev/null
	if (( $+functions[_al_f_] )); then
		zle accept-line
	else
		zle self-insert-unmeta
	fi
}
# Credits to: https://unix.stackexchange.com/questions/750378/disabling-the-secondary-prompt-in-zsh
