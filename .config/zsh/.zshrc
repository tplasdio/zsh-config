#                   ██
#                  ░██
#    ██████  ██████░██      ██████  █████
#   ░░░░██  ██░░░░ ░██████ ░░██░░█ ██░░░██
#      ██  ░░█████ ░██░░░██ ░██ ░ ░██  ░░
# ██  ██    ░░░░░██░██  ░██ ░██   ░██   ██
#░██ ██████ ██████ ░██  ░██░███   ░░█████
#░░ ░░░░░░ ░░░░░░  ░░   ░░ ░░░     ░░░░░

# Opciones
setopt CORRECT  # Corregir typos
setopt INTERACTIVE_COMMENTS  # Habilitar comentarios
setopt HIST_IGNORE_SPACE  # No mandar a historial comandos que empiecen con espacios
setopt EXTENDEDGLOB NOMATCH AUTOCD PROMPT_SUBST
unsetopt BEEP

disable r

SAVEHIST=1000
HISTSIZE=1000
HISTFILE="${XDG_DATA_HOME}/zsh/zsh_history"

SHELL_CURRENT="$(readlink /proc/$$/exe)"

# Evitar y eliminar duplicados en PATH:
source "${ZDOTDIR}/functions/path_append"
source "${ZDOTDIR}/functions/path_prepend"

# Prompts
source "${ZDOTDIR}/prompt/main.zsh"

export SPROMPT="$fg[red]✗ %R ¿Corregir $fg[red]%R$reset_color por $fg[green]%r$reset_color? [Yes, No, Abort, Edit]: "

#Columnas para 'ps', o sea correr 'ps -eo $pscols'
#Columnas para 'findmnt' o sea correr 'findmnt -o $fscols'
pscols="pid,user,stat,pcpu,pmem,comm,cmd" \
fscols="SOURCE,TARGET,FSTYPE,SIZE,USED,USE%" \
datef='+%d/%m/%Y %H:%M:%S'

# Aliases
source "$ZDOTDIR/aliases/main.sh"
source "$ZDOTDIR/aliases/rc.sh"
source "$ZDOTDIR/aliases/nonposix.sh"
source "$ZDOTDIR/aliases/xdgfix.sh"

# Colores
autoload U colors && colors

# Syntax Highlighting
source "$ZDOTDIR/plugins/syntaxhi.zsh"

# Autosuggestions
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

# Vi-mode
function zvm_config {
  ZVM_VI_INSERT_ESCAPE_BINDKEY='ml'  # how to make it 'mñ'?
}

source "$ZDOTDIR/plugins/zsh-vi-mode.plugin.zsh"

zvm_bindkey vicmd 'j' backward-char
zvm_bindkey vicmd 'ñ' forward-char
zvm_bindkey vicmd 'k' up-line-or-history
zvm_bindkey vicmd 'l' down-line-or-history
zvm_bindkey vicmd 'W' backward-word
#zvm_bindkey vicmd 'w' backward-blank-word
zvm_bindkey vicmd 'E' forward-word
#zvm_bindkey vicmd 'e' forward-blank-word
#zvm_bindkey vicmd 'b' forward-word-end
#zvm_bindkey vicmd 'B' backward-word-end

# Multiline editing
source "$ZDOTDIR/functions/accept-or-break-line.zsh"
zle -N accept-or-break-line
# FIXME: <Enter> accepts command with autosuggestion, but this one gets expanded altho it wasn't executed
bindkey '^M' accept-or-break-line  # Linebreak or accept command on <Enter>
bindkey '^[^M' self-insert-unmeta  # Linebreak on <Alt-Enter>
bindkey '^J' accept-line           # Accept command on <Ctrl-j>

# Edit cmdline in $EDITOR
autoload -Uz edit-command-line
zle -N edit-command-line
bindkey "^X^E" edit-command-line

# Menu-completion
zstyle ':completion:*' menu select
zmodload zsh/complist
bindkey -M menuselect 'j' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-down-line-or-history
bindkey -M menuselect 'ñ' vi-forward-char

autoload -Uz compinit
compinit
#autoload bashcompinit
#bashcompinit

# Para g.sh
source "$ZDOTDIR/plugins/gsh.sh"
source /usr/share/g/g_source.sh

# Interactive functions
source "$ZDOTDIR/functions/fcd"
source "$ZDOTDIR/functions/fcdd"
source "$ZDOTDIR/functions/cdd"
source "$ZDOTDIR/functions/cdf"
source "$ZDOTDIR/functions/cls"
source "$ZDOTDIR/functions/cll"
source "$ZDOTDIR/functions/cla"
source "$ZDOTDIR/functions/mkcd"
source "$ZDOTDIR/functions/up"
source "$ZDOTDIR/functions/varcheck"
