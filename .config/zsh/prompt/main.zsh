precmd_new_pwd() {
	local \
		pwdmaxlen=70 \
		trunc_symbol="…" \
		dir=${PWD##*/}

	pwdmaxlen=$(( ( pwdmaxlen < ${#dir} ) ? ${#dir} : pwdmaxlen ))
	NEW_PWD=${PWD/#$HOME/\}
	local pwdoffset=$(( ${#NEW_PWD} - pwdmaxlen ))
	if [ ${pwdoffset} -gt "0" ]; then
		NEW_PWD=${NEW_PWD:$pwdoffset:$pwdmaxlen}
		NEW_PWD=${trunc_symbol}/${NEW_PWD#*/}
	fi

	PS1=$'%{\e[48;2;35;38;39m%} '"$NEW_PWD"$' %{\e[38;2;35;38;39;48;2;202;31;31m%}%{\e[38;2;202;31;31;49m%}
%{\e[48;2;35;38;39;38;2;23;147;209m%}  %{\e[38;2;35;38;39;48;2;202;31;31m%}%{\e[38;2;202;31;31;49m%}%{\e[0m%} '
}
precmd_new_pwd
chpwd_functions+=(precmd_new_pwd)

RPS1='$(_ES=$?; (($_ES!=0)) && printf "%%{\e[38;5;235m%%}%%{\e[1;38;5;208;48;5;235m%%} $_ES%%{\e[38;5;1;48;5;235m%%}\342\234\227%%{\e[m%%}" || printf "%%{\e[38;5;235m%%}%%{\e[1;38;5;47;48;5;235m%%} \342\234\223%%{\e[m%%}")'
