if source /usr/share/zsh/plugins/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh 2>/dev/null; then
	if ! [[ ${"$(fast-theme -s)"##* } == *myhi* ]]; then
		fast-theme "$ZDOTDIR/plugins/myhi.ini"
	fi
elif source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null; then
	typeset -A ZSH_HIGHLIGHT_STYLES=(
		# can one use italics?
		[command]='fg=#54ff54'
		[alias]='fg=51'
		[path]='underline'
		[path_prefix]='fg=#438eff,underline'
		[globbing]='fg=198'
		[reserved-word]='fg=#F81E16,bold'
		[builtin]='fg=#ff442b'
		[precommand]='fg=#ff442b'
		[comment]='fg=#7f7f7f,italic'
		[single-hyphen-option]='fg=#18b2b2'
		[double-hyphen-option]='fg=#18b2b2'
		[commandseparator]='fg=220'
		[redirection]='fg=220'
		[process-substitution-delimiter]='fg=214'
		[single-quoted-argument]='fg=green'
		[double-quoted-argument]='fg=green'
		[dollar-quoted-argument]='fg=#54ff54'
		[function]='fg=#e11ee1'
		[history-expansion]='fg=#A017A2'
		[command-substitution-delimiter]='fg=#d11ee2'
		[back-quoted-argument-delimiter]='fg=#d11ee2'
		[dollar-double-quoted-argument]='fg=#ff9500,bold'
		[assign]='fg=33'
		[unknown-token]='fg=red,bold,underline'
	)
fi
