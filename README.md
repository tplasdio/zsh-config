<p align="center">
  <h1 align="center">zsh-config</h1>
  <p align="center">
    My WIP zsh configurations
  </p>
</p>

Note: this config is *not* as good as my
[`bash-config`](https://codeberg.org/tplasdio/bash-config),
which I recommend.

## Installation

- Set `ZDOTDIR` to `~/.config/zsh`
- Clone and copy my `dash-config` and `bash-config` repos to your system
- Clone and copy this repo to your system
- Install plugins (I install and source them manually, no plugin manager):
  - `zsh-fast-syntax-highligting` or `zsh-syntax-highligting`
  - `zsh-autosuggestions`
  - `zsh-vi-mode`

## To-do

- [-] Multiline editing like `ble.sh` or `fish`
- [ ] Better remapping of zvm (vi-mode) keys
- [ ] Vim command-mode `!`
- [-] Better tokenization for syntax highlighting
- [ ] Status line like ble.sh
- [ ] History expansion like abbreviations
- [ ] zprofile

<!--
zsh plugins would have to catch up to ble.sh if I were to ever migrate,
zsh as a shell language and project is cool, but plugins are not quite
there for me yet; ble.sh is not perfect but it's better for my needs
-->
